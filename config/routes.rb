Rails.application.routes.draw do
  devise_for :users

  authenticate :user do
    resources :job_openings, only: [:index, :show, :new, :create]
    resources :job_applications, only: [:create]
  end

  devise_scope :user do
    root to: "devise/sessions#new"
  end
end
