class User < ApplicationRecord
  devise :database_authenticatable, :validatable

  has_one :employer
  has_one :candidate

  def candidate?
    !!candidate
  end

  def employer?
    !!employer
  end
end
