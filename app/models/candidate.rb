class Candidate < ApplicationRecord
	belongs_to :user, optional: true

	has_many :job_applications
	has_many :job_openings, through: :job_applications

	validates :first_name, presence: true, allow_blank: false
	validates :surname, presence: true, allow_blank: false

	def applied_to?(job_opening)
		job_openings.include?(job_opening)
	end
end
