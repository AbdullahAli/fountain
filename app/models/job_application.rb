class JobApplication < ApplicationRecord
	belongs_to :candidate
	belongs_to :job_opening

	validates :candidate, uniqueness: { scope: :job_opening, message: 'already applied' }
end
