class Employer < ApplicationRecord
	belongs_to :user, optional: true

	validates :name, presence: true, allow_blank: false
end
