class JobOpening < ApplicationRecord
	belongs_to :employer

	has_many :job_applications
	has_many :candidates, through: :job_applications

	scope :for_employer, ->(employer) { where(employer: employer) }

	validates :title, presence: true, allow_blank: false
	validates :description, presence: true, allow_blank: false
end
