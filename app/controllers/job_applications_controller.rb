class JobApplicationsController < ApplicationController
  before_action :authenticate_candidate!

  def create
    @job_application = JobApplication.new(job_application_params)
    @job_application.candidate = current_user.candidate

    if @job_application.save
      redirect_to job_openings_path, :flash => { success: "Job application was successfully created" }
    else
      redirect_to job_openings_path, :flash => { error: @job_application.errors.full_messages.first }
    end
  end

  private
    def job_application_params
      params.permit(:job_opening_id)
    end
end
