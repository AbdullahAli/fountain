class JobOpeningsController < ApplicationController
  before_action :authenticate_user!, only: [:index]
  before_action :authenticate_employer!, only: [:new, :create, :show]

  before_action :set_job_opening, only: [:show]

  def index
    if current_user.candidate?
      @job_openings = JobOpening.all.includes(:employer)
    elsif current_user.employer?
      @job_openings = JobOpening.for_employer(current_user.employer)
    end
  end

  def new
    @job_opening = JobOpening.new
  end

  def create
    @job_opening = JobOpening.new(job_opening_params)
    @job_opening.employer = current_user.employer

    if @job_opening.save
      flash[:success] = 'Job opening was successfully created'
      redirect_to job_openings_path
    else
      render :new
    end
  end

  private
    def set_job_opening
      @job_opening = JobOpening.find(params[:id])
    end

    def job_opening_params
      params.require(:job_opening).permit(:title, :description)
    end
end
