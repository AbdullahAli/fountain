class ApplicationController < ActionController::Base
	before_action :authenticate_user!

	def authenticate_candidate!
		authenticate_by_user_type!(:candidate)
	end

	def authenticate_employer!
		authenticate_by_user_type!(:employer)
	end

	private
		def authenticate_by_user_type!(user_type)
			authenticate_user!

			if !current_user.public_send("#{user_type}?")
				flash[:error] = "Only #{user_type}s can do this"
				redirect_back fallback_location: job_openings_path
			end
		end

		def after_sign_in_path_for(resource)
			job_openings_path
		end
end
