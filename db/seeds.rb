password = "hello123"

ActiveRecord::Base.transaction do
	User.delete_all
	Candidate.delete_all
	Employer.delete_all
	JobOpening.delete_all
	JobApplication.delete_all

	abdullah = Candidate.create!(first_name: 'Abdullah', surname: 'Ali', user: User.create!(email: "hello@abdullahali.com", password: password, password_confirmation: password))

	john = Candidate.create!(first_name: 'John', surname: 'Doe', user: User.create!(email: "hello@johndoe.com", password: password, password_confirmation: password))

	fountain = Employer.create!(name: "Fountain", user: User.create!(email: "hello@fountain.com", password: password, password_confirmation: password))

	john_lewis = Employer.create!(name: "John Lewis", user: User.create!(email: "hello@johnlewis.com", password: password, password_confirmation: password))

	fountain_job_opening = JobOpening.create!(title: "Full Stack Developer", description: "Awsome stuff", employer: fountain)

	JobOpening.create!(title: "Operational Manager", description: "Cool", employer: john_lewis)

	JobApplication.create!(candidate: abdullah, job_opening: fountain_job_opening)
end