class CreateCandidates < ActiveRecord::Migration[5.2]
  def change
    create_table :candidates do |t|
      t.string :first_name
      t.string :surname
      t.integer :user_id

      t.timestamps
    end
  end
end
