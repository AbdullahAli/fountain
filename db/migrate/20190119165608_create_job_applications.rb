class CreateJobApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :job_applications do |t|
      t.integer :job_opening_id
      t.integer :candidate_id

      t.timestamps
    end

    add_index :job_applications, [:job_opening_id, :candidate_id], unique: true
  end
end
