class CreateJobOpenings < ActiveRecord::Migration[5.2]
  def change
    create_table :job_openings do |t|
      t.string :title
      t.text :description
      t.integer :employer_id

      t.timestamps
    end

    add_index :job_openings, :employer_id
  end
end
