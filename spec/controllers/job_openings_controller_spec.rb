require 'spec_helper'

describe JobOpeningsController, :type => :controller do
  describe "GET 'index'" do
    subject { get :index }

    context "with user not signed in" do
      it "does not allow the user to view the page" do
        expect(subject).not_to be_success
        expect(subject).to redirect_to(new_user_session_path)
      end
    end

    context "with candidate signed in" do
      before { sign_in(create(:candidate_user)) }
      let!(:job_openings) { [create(:job_opening), create(:job_opening)]}

      it "allows the candidate user to view the page" do
        expect(subject).to be_success
      end

      it "allows the candidate user to view all of the job openings" do
        subject
        expect(assigns(:job_openings)).to match_array(job_openings)
      end
    end

    context "with employer signed in" do
      let(:employer_user) { create(:employer_user) }
      let(:employer_job_opening) { create(:job_opening, employer: employer_user.employer) }

      before do
        sign_in(employer_user)
        # create a job opening by another employer
        create(:job_opening)
      end

      it "allows the employer user to view the page" do
        expect(subject).to be_success
      end

      it "allows the employer user to view their job openings only" do
        subject
        expect(assigns(:job_openings)).to match_array(employer_job_opening)
      end
    end
  end

  describe "GET 'new'" do
    subject { get :new }

    context "with candidate signed in" do
      before { sign_in(create(:candidate_user)) }

      it "does not allow the candidate to view the page" do
        expect(subject).not_to be_success
        expect(subject).to redirect_to(job_openings_path)
      end
    end

    context "with employer signed in" do
      before { sign_in(create(:employer_user)) }

      it "allows the employer to view the page" do
        expect(subject).to be_success
      end

      it "sets up a new job opening" do
        job_opening = JobOpening.new
        allow(JobOpening).to receive(:new).and_return(job_opening)
        subject
        expect(assigns(:job_opening)).to eq(job_opening)
      end
    end
  end

  describe "POST 'create'" do
    let(:params) {{
      job_opening: {
        title: 'Full Stack Developer',
        description: 'Awesome job'
      }
    }}

    subject { post :create, params: params }

    context "with candidate signed in" do
      before { sign_in(create(:candidate_user)) }

      it "does not allow the candidate to create a new job opening" do
        expect(subject).not_to be_success
        expect(subject).to redirect_to(job_openings_path)
      end
    end

    context "with employer signed in" do
      before { sign_in(create(:employer_user)) }

      context "with invalid params" do
        context "with missing title" do
          let(:missing_title_params) {{
            job_opening: {
              description: 'Awesome job'
            }
          }}

          subject { post :create, params: missing_title_params }

          it "does not create a new job opening" do
            expect{ subject }.not_to change(JobOpening, :count)
          end

          it "renders the new page" do
            expect(subject).to render_template(:new)
          end
        end

        context "with missing description" do
          let(:missing_description_params) {{
            job_opening: {
              title: 'Full Stack developer'
            }
          }}

          subject { post :create, params: missing_description_params }

          it "does not create a new job opening" do
            expect{ subject }.not_to change(JobOpening, :count)
          end
        end
      end

      context "with valid params" do
        it "creates a new job opening" do
          expect{ subject }.to change(JobOpening, :count).by(1)
          job_opening = JobOpening.last
          expect(job_opening.title).to eql(params[:job_opening][:title])
          expect(job_opening.description).to eql(params[:job_opening][:description])
        end

        it "redirects to index page" do
          expect(subject).to redirect_to(job_openings_path)
          expect(flash[:success]).to match("Job opening was successfully created")
        end
      end
    end
  end

  describe "GET 'show'" do
    let(:job_opening) { create(:job_opening) }
    subject { get :show, params: { id: job_opening }  }

    context "with candidate signed in" do
      before { sign_in(create(:candidate_user)) }

      it "does not allow the candidate to view the page" do
        expect(subject).not_to be_success
        expect(subject).to redirect_to(job_openings_path)
      end
    end

    context "with employer signed in" do
      before { sign_in(create(:employer_user)) }

      it "allows the employer to view the page" do
        expect(subject).to be_success
      end

      it "assigns job_opening" do
        subject
        expect(assigns(:job_opening)).to eq(job_opening)
      end
    end
  end
end