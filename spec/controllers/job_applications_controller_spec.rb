require 'spec_helper'

describe JobApplicationsController, :type => :controller do
	describe "POST 'create'" do
		let(:job_opening) { create(:job_opening) }
    let(:params) {{
			job_opening_id: job_opening.id
    }}

    subject { post :create, params: params }

    context "with employer signed in" do
      before { sign_in(create(:employer_user)) }

      it "does not allow the employer to create a new job application" do
        expect(subject).not_to be_success
        expect(subject).to redirect_to(job_openings_path)
      end
    end

		context "with candidate signed in" do
			let(:candidate_user) { create(:candidate_user) }
      before { sign_in(candidate_user) }

      context "with invalid params" do
        context "with missing title" do
          let(:missing_job_opening_id_params) {{}}

          subject { post :create, params: missing_job_opening_id_params }

          it "does not create a new job opening" do
            expect{ subject }.not_to change(JobApplication, :count)
          end

          it "redirects to the job openings index page" do
						expect(subject).to redirect_to(job_openings_path)
          	expect(flash[:error]).to match("Job opening must exist")
          end
        end
      end

      context "with valid params" do
				it "creates a new job application" do
          expect{ subject }.to change(JobApplication, :count).by(1)
          job_application = JobApplication.last
          expect(job_application.job_opening).to eql(job_opening)
          expect(job_application.candidate).to eql(candidate_user.candidate)
        end

				it "redirects to the job openings index page" do
					expect(subject).to redirect_to(job_openings_path)
          expect(flash[:success]).to eq("Job application was successfully created")
        end
      end
    end
  end
end