require 'spec_helper'

describe Candidate, type: :model do
	let(:candidate) { create(:candidate) }

	describe "associations" do
		it "belongs to a user" do
			association = described_class.reflect_on_association(:user)
      expect(association.macro).to eq :belongs_to
		end

		it "has many job applications" do
			association = described_class.reflect_on_association(:job_applications)
      expect(association.macro).to eq :has_many
		end

		it "has many job openings" do
			association = described_class.reflect_on_association(:job_openings)
      expect(association.macro).to eq :has_many
		end
	end

	describe "validations" do
		it "is valid with valid attributes" do
      expect(candidate).to be_valid
		end

		context "with missing first_name" do
			it "is not valid without a first_name" do
				candidate.first_name = nil
				expect(candidate).to_not be_valid
			end

			it "is not valid without a blank first_name" do
				candidate.first_name = ''
				expect(candidate).to_not be_valid
			end
		end

		context "with missing surname" do
			it "is not valid without a surname" do
				candidate.surname = nil
				expect(candidate).to_not be_valid
			end

			it "is not valid without a blank surname" do
				candidate.surname = ''
				expect(candidate).to_not be_valid
			end
		end
	end

	describe "#applied_to?" do
		let(:job_opening) { create(:job_opening) }

		context "without a job application for the job opening" do
			it "is false" do
				expect(candidate.applied_to?(job_opening)).to be(false)
			end
		end

		context "with job application existing for the job opening" do
			it "is true" do
				candidate.job_openings << job_opening
				expect(candidate.applied_to?(job_opening)).to be(true)
			end
		end
	end
end