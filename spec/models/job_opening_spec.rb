require 'spec_helper'

describe JobOpening, type: :model do
	let(:job_opening) { create(:job_opening) }

	describe "associations" do
		it "belongs to an employer" do
			association = described_class.reflect_on_association(:employer)
      expect(association.macro).to eq :belongs_to
		end

		it "has many job applications" do
			association = described_class.reflect_on_association(:job_applications)
      expect(association.macro).to eq :has_many
		end
	end

	describe "scopes" do
		describe ".for_employer" do
			let(:fountain_employer) { create(:employer) }
			let(:john_lewis_employer) { create(:employer) }

			let!(:fountain_job_openings) { create(:job_opening, employer: fountain_employer) }
			let!(:john_lewis_job_openings) { create(:job_opening, employer: john_lewis_employer) }

			it "returns only the job openings for specified employer" do
				expect(JobOpening.for_employer(fountain_employer)).to match_array([fountain_job_openings])
			end
		end
	end

	describe "validations" do
		it "is valid with valid attributes" do
      expect(job_opening).to be_valid
		end

		context "with missing title" do
			it "is not valid without a title" do
				job_opening.title = nil
				expect(job_opening).to_not be_valid
			end

			it "is not valid without a blank title" do
				job_opening.title = ''
				expect(job_opening).to_not be_valid
			end
		end

		context "with missing description" do
			it "is not valid without a description" do
				job_opening.description = nil
				expect(job_opening).to_not be_valid
			end

			it "is not valid without a blank description" do
				job_opening.description = ''
				expect(job_opening).to_not be_valid
			end
		end
	end
end