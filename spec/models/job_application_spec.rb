require 'spec_helper'

describe JobApplication, type: :model do
	let(:job_application) { create(:job_application) }

	describe "associations" do
		it "belongs to a job opening" do
			association = described_class.reflect_on_association(:job_opening)
      expect(association.macro).to eq :belongs_to
		end

		it "belongs to a candidate" do
			association = described_class.reflect_on_association(:candidate)
      expect(association.macro).to eq :belongs_to
		end
	end

	describe "validations" do
		it "is valid with valid attributes" do
      expect(job_application).to be_valid
		end

		context "multiple job applications to the same job opening" do
			it "is not valid" do
				job_application_candidate = job_application.candidate
				job_application_job_opening = job_application.job_opening

				new_job_application = JobApplication.new(job_opening: job_application_job_opening, candidate: job_application_candidate)

        expect(new_job_application).not_to be_valid
			end
		end
	end
end