require 'spec_helper'

describe User, type: :model do
	let(:user) { create(:user) }

	describe "associations" do
		it "has one employer" do
			association = described_class.reflect_on_association(:employer)
      expect(association.macro).to eq :has_one
		end

		it "has one candidate" do
			association = described_class.reflect_on_association(:candidate)
      expect(association.macro).to eq :has_one
		end
	end

	describe "candidate?" do
		context "with user not as a candidate user" do
			let(:employer_user) { create(:employer_user) }

			it "is false" do
				expect(employer_user.candidate?).to be(false)
			end
		end

		context "with user as a candidate user" do
			let(:candidate_user) { create(:candidate_user) }

			it "is true" do
				expect(candidate_user.candidate?).to be(true)
			end
		end
	end

	describe "employer?" do
		context "with user not as a employer user" do
			let(:candidate_user) { create(:candidate_user) }

			it "is false" do
				expect(candidate_user.employer?).to be(false)
			end
		end

		context "with user as a employer user" do
			let(:employer_user) { create(:employer_user) }

			it "is true" do
				expect(employer_user.employer?).to be(true)
			end
		end
	end
end