require 'spec_helper'

describe Employer, type: :model do
	let(:employer) { create(:employer) }

	describe "associations" do
		it "belongs to a user" do
			association = described_class.reflect_on_association(:user)
      expect(association.macro).to eq :belongs_to
		end
	end

	describe "validations" do
		it "is valid with valid attributes" do
      expect(employer).to be_valid
		end

		context "with missing name" do
			it "is not valid without a name" do
				employer.name = nil
				expect(employer).to_not be_valid
			end

			it "is not valid without a blank name" do
				employer.name = ''
				expect(employer).to_not be_valid
			end
		end
	end
end