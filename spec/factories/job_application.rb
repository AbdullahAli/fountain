FactoryBot.define do
  factory :job_application do
		association :candidate
    association :job_opening
  end
end
