FactoryBot.define do
  factory :user do
    sequence(:email) {|n| "user#{n}@example.com" }
    password 'password'
    password_confirmation 'password'
  end

  factory :candidate_user, :class => 'User' do
    sequence(:email) {|n| "admin#{n}@example.com" }
    password 'password'
    password_confirmation 'password'
    association :candidate
  end

  factory :employer_user, :class => 'User' do
    sequence(:email) {|n| "admin#{n}@example.com" }
    password 'password'
    password_confirmation 'password'
    association :employer
  end
end
