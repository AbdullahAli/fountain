FactoryBot.define do
  factory :candidate do
    sequence(:first_name) {|n| "Abdullah#{n}" }
    sequence(:surname) {|n| "Ali#{n}" }
  end
end
