FactoryBot.define do
  factory :employer do
    sequence(:name) {|n| "Fountain#{n}" }
  end
end
