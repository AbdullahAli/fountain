FactoryBot.define do
  factory :job_opening do
		sequence(:title) {|n| "Full Stack Developer#{n}" }
		sequence(:description) {|n| "Awesome role#{n}" }
    association :employer
  end
end
