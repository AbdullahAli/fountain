# Fountain

# Instructions
## Installation
```
git clone git@bitbucket.org:AbdullahAli/fountain.git
cd fountain
bundle install
rails db:create && rails db:migrate && rails db:seed
rspec spec
rails server
```
All the test should pass and the server should start at

```
http://localhost:3000
```

# Description
The application can be best described in terms of its models, which are associated as follows

![models](models.png)

Note: association fields have been omited for simplicy

## Models
As you can see, there is a concept of a `user`, a `candidate` and an `employer`.  This was introduced to simplify how [Devise](https://github.com/plataformatec/devise) handles authentication.  It is simpler to handle a single model for which devise can authenticate against.  An alternative solution would be to allow devise use two models to handle authentication, however, for this small applied it might prove overkill as it is unnecessarily complicated

After logging in, we can simply use the created helper methods `user.candidate?` and `user.employer?` to handle role based authorisations.  For this simple application, a gem like [cancancan](https://github.com/CanCanCommunity/cancancan) is overkill.  However, for a bigger application, this would be a good idea

## Tests
The tests provided are done using [RSpec](https://github.com/rspec/rspec-rails).  These unit tests tests all aspects of the models, including associations, validations, scopes and instance methods

BDD test would also be useful (however, I didn't have time to write them).  Any [Cucumber](https://github.com/cucumber/cucumber-rails) flavor is usually fine, including [Gherkin](https://github.com/codegram/gherkin-ruby) and [Turnip](https://github.com/jnicklas/turnip)

## Backend/Frontend vs Monolith
Rails makes it incredibly easy to get started with a Monolith application, which is what I opted to use for this simple task.  However, in a real-life production system, it might make more sense to split the application to an API Backend (eg: using Rails' `--api` [option](https://guides.rubyonrails.org/api_app.html)).  This can then be authenticated against using a token based authentication gem such as [Devise-JWT](https://github.com/waiting-for-dev/devise-jwt).  On the frontend, we can then use any technology we like, such as [React](https://reactjs.org/), [Angular](https://angular.io/) or [Vue](https://vuejs.org/)

## UI
A very basic UI has been provided using bootstrap (😭)

# Usage
Go to the browser at `http://localhost:3000` and login.

You have four log in accounts:

## Employer account

As an employer you can view your own job openings and create new ones.  Additionally you can view the candidates that applied to these job openings.  An employer can not view other employer's job openings and can not apply as a candidate

Employer account 1
```
username: hello@fountain.com
password: hello123
```

Employer account 2
```
username: hello@johnlewis.com
password: hello123
```

## Candidate account

As a candidate you can view all of the employers' job openings and applied to the job opening once.  A candidate can not create a new job opening

Candidate account 1
```
username: hello@abdullahali.com
password: hello123
```

Candidate account 2
```
username: hello@johndoe.com
password: hello123
```

